package com.planetjumper;

/**
 * This class is part of the "Planet Jumper" application. 
 * "Planet Jumper" is a text based game that involves exploring the solar system in order to 
 * find minerals and bring them back to earth to help the survival of humanity. Users can
 * jump from planets to moons in the solar system searching for minerals and resources. 
 * To win this game the user must find a quantity of 100 gold, silver and copper then be 
 * able to make it back to earth with enough fuel, water and air to survive the journey. 
 * Exploring the solar to give the minerals to the people of earth will help the human race 
 * from depleting their minerals.
 * 
 * This class holds an enumeration of all command words known to the game.
 * It is used to recognise commands as they are typed in.
 *
 * @author  Bryn Flewelling
 * @version 2010.10.07
 */

public class CommandWords
{
    // a constant array that holds all valid command words
    private static final String[] validCommands = {
        "jump", "display", "scan", "get", "map", "cargo", "back", "quit", "help", "scavenge"
    };
    /**
     * Constructor - initialise the command words.
     */
    public CommandWords()
    {
        // nothing to do at the moment...
    }
    /**
     * Check whether a given String is a valid command word. 
     * @return true if it is, false if it isn't.
     */
    public boolean isCommand(String aString)
    {
        for(int i = 0; i < validCommands.length; i++) {
            if(validCommands[i].equals(aString))
                return true;
        }
        // if we get here, the string was not found in the commands
        return false;
    }
    /**
     * Print all valid commands to System.out.
     */
    public void showAll() 
    {
        for(String command: validCommands) {
            System.out.print(command + "  ");
        }
        System.out.println();
    }
}
