package com.planetjumper;

/**
	 * Class Gold - contains the quantity and the ItemName of gold.
	 *
	 * This class is part of the "Planet Jumper" application. 
	 * "Planet Jumper" is a text based game that involves exploring the solar system in order to 
	 * find minerals and bring them back to earth to help the survival of humanity. Users can
	 * jump from planets to moons in the solar system searching for minerals and resources. 
	 * To win this game the user must find a quantity of 100 gold, silver and copper then be 
	 * able to make it back to earth with enough fuel, water and air to survive the journey. 
	 * Exploring the solar to give the minerals to the people of earth will help the human race 
	 * from depleting their minerals. 
	 *
	 * A "Room" represents one location in the scenery of the game.  It is 
	 * connected to other rooms via exits.  For each existing exit, the room 
	 * stores a reference to the neighboring room.
	 * 
	 * @author  Bryn Flewelling
	 * @version 2010.10.07
	 */

public class Gold extends Item {
	
	public Gold(int quantity, String itemName) {
		super(quantity, itemName);
	}
}
