package com.planetjumper;

//import java.util.Iterator;
import java.util.Set;
import java.util.HashMap;

/**
 * Class Planet - a planet in a space exploration game.
 *
 * This class is part of the "Planet Jumper" application. 
 * "Planet Jumper" is a text based game that involves exploring the solar system in order to 
 * find minerals and bring them back to earth to help the survival of humanity. Users can
 * jump from planets to moons in the solar system searching for minerals and resources. 
 * To win this game the user must find a quantity of 100 gold, silver and copper then be 
 * able to make it back to earth with enough fuel, water and air to survive the journey. 
 * Exploring the solar to give the minerals to the people of earth will help the human race 
 * from depleting their minerals.   
 *
 * A "Planet" represents one coordinate in the solar system of the game. It is 
 * connected to other planets and moons via exits.  For each existing exit, it 
 * stores a reference to the neighbouring planet or moon.
 * 
 * @author  Bryn Flewelling
 * @version 2010.10.07
 */

public class Planet     
{
	private String name;
	protected HashMap<String, Planet> coordinate; // stores coordinates of planets.
	private int distanceFromSun;
	private HashMap<String, Moon> moons; 
	private Boolean isCurrentCoordinate = false;
	protected Fuel fuel;
	protected Air air;
	protected Water water;
	protected Gold gold;
	protected Silver silver;
	protected Copper copper;

	/**
	 * Create a planet name "name". 
	 * Initialise resources and minerals.
	 * @param name The planets name.
	 */
	public Planet(String name) 
	{
		this.name = name;
		coordinate = new HashMap<String, Planet>();
		moons = new HashMap<String, Moon>();
		//Life
		fuel = new Fuel(100,"Fuel");
		air  = new Air(50,"Air");
		water  = new Water(25,"Water");
		//Resources
		gold = new Gold(0,"Gold");
		silver  = new Silver(0,"Silver");
		copper  = new Copper(0,"Copper");
	}
	/**
	 * Sets the name of the moons.
	 * @param moon The Moon's name.
	 */
	public void setMoon(Moon moon)
	{
		moons.put(moon.getShortName(),moon);
	}
	/**
	 * Sets the coordinates.
	 * @param name The name of a planet.
	 * @param neighbor The planet to which the planet leads.
	 */
	public void setCoordinate(String name, Planet neighbor) 
	{
		coordinate.put(name, neighbor);
	}
	/**
	 * calculate distance from sun.
	 * @param distanceFromSun The current distance from the sun.
	 */
	public void setDistanceFromSun(int distanceFromSun) 
	{
		this.distanceFromSun = distanceFromSun;
	}
	/**
	 * Get the distance from sun.
	 * @return distanceFromSun The current distance from the sun.
	 */
	public int getDistanceFromSun() 
	{
		return distanceFromSun;
	}
	/**
	 * @return The short description of the planet
	 * (the one that was defined in the constructor).
	 */
	public String getShortName()
	{
		return name;
	}
	/**
	 * Return a name of the planet in the form:
	 *     Coordinate : Earth.
	 *     Coordinates: Mars.
	 * @return A long name of this planet
	 */
	public String getLongName()
	{
		return "You are currently at " + name + ".\n" + getCoordinateString();
	}

	/**
	 * Return a string describing the planet's coordinates, for example
	 * "Planets: Mars Jupiter.
	 * @return returnString The Details of the planets's coordinates.
	 */
	private String getCoordinateString()
	{
		//bug fix wont display every jump
		String returnString = "Coordinate:";
		Set<String> keys = coordinate.keySet();
		for(String coordinate : keys) {
			returnString += " " + coordinate;
			//Game.details.append("\n");
		}
		returnString += " \n";
		return returnString;
	}
	/**
	 * Gets the moons name.
	 * @param name The moons name.
	 * @return The moon by moon name.
	 */
	public Moon getMoon(String name)
	{
		return moons.get(name);
	}
	/**
	 * Displays a list of planets moons coordinates.
	 * @return The moons for a planet.
	 */
	public String displayMoons()
	{
		String moonList = "Moons: ";
		Set<String> keys = moons.keySet();
		for(String moons : keys) {
			moonList += moons + ", ";
		}
		moonList += " \n";
		return moonList;
	}
	/**
	 * Sets the current coordinate to true or false.
	 * @param value The coordinates value.
	 */
	public void setIsCurrentCoordinate(Boolean value)
	{
		this.isCurrentCoordinate = value;
	}  
	/**
	 * Gets the current coordinate of a planet or a moon.
	 * @return isCurrentCoordinate The current coordinate.
	 */
	public Boolean getIsCurrentCoordinate() {
		return isCurrentCoordinate;
	}
	/**
	 * Sets the fuel item. 
	 * @param fuel The item fuel.
	 */
	public void setFuel(Fuel fuel) {
		this.fuel = fuel;
	}
	/**
	 * Set the air item.
	 * @param air The item air.
	 */
	public void setAir(Air air) {
		this.air = air;
	}
	/**
	 * Sets the water item.
	 * @param water The item water.
	 */
	public void setWater(Water water) {
		this.water = water;
	}
	/**
	 * Sets the gold item.
	 * @param gold The item gold.
	 */
	public void setGold(Gold gold) {
		this.gold = gold;
	}
	/**
	 * Sets the silver item.
	 * @param silver The item silver.
	 */
	public void setSilver(Silver silver) {
		this.silver = silver;
	}
	/**
	 * Sets the copper item.
	 * @param copper The item copper.
	 */
	public void setCopper(Copper copper) {
		this.copper = copper;
	}
	/**
	 * Gets the quantity of fuel.
	 * @param amountOfFuel The fuels quantity.
	 * @return amountOfFuel The amount of fuel the user has.
	 * @return tmpFuel The fuel quantity.
	 */
	public int getFuel(int amountOfFuel) {
		if(fuel.getQuantity() - amountOfFuel > 0){
			int quantity = fuel.getQuantity() - amountOfFuel;
			fuel.setQuantity(quantity);  
			return amountOfFuel;	
		}else {
			int tmpFuel = fuel.getQuantity();
			fuel.setQuantity(0);
			Game.details.append("Desired Amount of fuel greater than available fuel. Returning avialable fuel : " + tmpFuel + "\n");

			return tmpFuel;
		}
	}
	/**
	 * Gets the quantity of air.
	 * @param amountOfAir The air's quantity.
	 * @return amountOfAir The amount of air the user has.
	 * @return tmpAir The Air quantity
	 */
	public int getAir(int amountOfAir) {
		if(air.getQuantity() - amountOfAir > 0){
			int quantity = fuel.getQuantity() - amountOfAir;
			air.setQuantity(quantity);  
			return amountOfAir;	
		}else {
			int tmpAir = air.getQuantity();
			air.setQuantity(0);
			Game.details.append("Desired Amount of air greater than available air. Returning avialable air : " + tmpAir + "\n");			
			return tmpAir;
		}
	}
	/**
	 * Gets the water quantity.
	 * @param amountOfWater The waters quantity.
	 * @return amountOfWater The amount of water the user has.
	 * @return tmpWater The water quantity.
	 */
	public int getWater(int amountOfWater) {
		if(water.getQuantity() - amountOfWater > 0){
			int quantity = water.getQuantity() - amountOfWater;
			water.setQuantity(quantity);  
			return amountOfWater;	
		}else {
			int tmpWater = water.getQuantity();
			water.setQuantity(0);
			Game.details.append("Desired Amount of water greater than available water. Returning avialable water : " + tmpWater + "\n");		
			return tmpWater;
		}	
	}
	/**
	 * Gets the gold quantity.
	 * @param amountOfGold The quantity of gold.
	 * @return amountOfGold The amount of gold the user has.
	 * @return tmpGold The gold quantity. 
	 */
	public int getGold(int amountOfGold) {
		if(gold.getQuantity() - amountOfGold > 0){
			int quantity = gold.getQuantity() - amountOfGold;
			gold.setQuantity(quantity);  
			return amountOfGold;
		}else {
			int tmpGold = gold.getQuantity();			
			gold.setQuantity(0);
			Game.details.append("Desired Amount of gold greater than available gold. Returning avialable gold : " + tmpGold + "\n");		
			return tmpGold;
		}
	}
	/**
	 * Gets the silver quantity.
	 * @param amountOfSilver The quantity of silver.
	 * @return amountOfSilver The amount of silver the user has.
	 * @return tmpSilver The silver quantity. 
	 */
	public int getSilver(int amountOfSilver) {
		if(silver.getQuantity() - amountOfSilver > 0){
			int quantity = silver.getQuantity() - amountOfSilver;
			silver.setQuantity(quantity);  
			return amountOfSilver;	
		}else {
			int tmpSilver = silver.getQuantity();
			silver.setQuantity(0);
			Game.details.append("Desired Amount of silver greater than available silver. Returning avialable silver : " + tmpSilver + "\n");		
			return tmpSilver;
		}
	}
	/**
	 * Gets the copper quantity.
	 * @param amountOfCopper The quantity of copper.
	 * @return amountOfCopper The amount of copper the user has.
	 * @return tmpCopper The copper quantity.
	 */
	public int getCopper(int amountOfCopper) {
		if(copper.getQuantity() - amountOfCopper > 0){
			int quantity = copper.getQuantity() - amountOfCopper;
			copper.setQuantity(quantity);  
			return amountOfCopper;	
		}else {
			int tmpCopper = copper.getQuantity();
			copper.setQuantity(0);
			Game.details.append("Desired Amount of copper greater than available copper. Returning avialable copper : " + tmpCopper + "\n");		
			return tmpCopper;
		}
	}
	/**
	 * Gets the planets coordinate.
	 * @param name The planets name.
	 * @return planet The coordinates name.
	 */
	public Planet getCoordinate(String name) 
	{
		return coordinate.get(name);
	}
}
