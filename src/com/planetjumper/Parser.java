package com.planetjumper;

import java.util.Scanner;

/**
 * This class is part of the "Planet Jumper" application. 
 * "Planet Jumper" is a text based game that involves exploring the solar system in order to 
 * find minerals and bring them back to earth to help the survival of humanity. Users can
 * jump from planets to moons in the solar system searching for minerals and resources. 
 * To win this game the user must find a quantity of 100 gold, silver and copper then be 
 * able to make it back to earth with enough fuel, water and air to survive the journey. 
 * Exploring the solar to give the minerals to the people of earth will help the human race 
 * from depleting their minerals.
 * 
 * This parser reads user input and tries to interpret it as an "Adventure"
 * command. Every time it is called it reads a line from the terminal and
 * tries to interpret the line as a two word command. It returns the command
 * as an object of class Command.
 *
 * The parser has a set of known command words. It checks user input against
 * the known commands, and if the input is not one of the known commands, it
 * returns a command object that is marked as an unknown command.
 * 
 * @author  Bryn Flewelling
 * @version 2010.10.07
 */

public class Parser 
{
	private CommandWords commands;  // holds all valid command words
	private Scanner reader;         // source of command input

	/**
	 * Create a parser to read from the terminal window.
	 */
	public Parser() 
	{
		commands = new CommandWords();
		reader = new Scanner(System.in);
	}

	/**
	 * @return The next command from the user.
	 */
	public Command getCommand() 
	{
		String inputLine;   // will hold the full input line

		System.out.print("> ");     // print prompt

		inputLine = reader.nextLine();
		return parseInput(inputLine);


	}
	public Command parseInput (String inputLine) {
		// Find up to two words on the line.
		String word1 = null;
		String word2 = null;
		String word3 = null;
		Scanner tokenizer = new Scanner(inputLine);
		if(tokenizer.hasNext()) {
			word1 = tokenizer.next();      // get first word
			if(tokenizer.hasNext()) {
				word2 = tokenizer.next();      // get second word
				// note: we just ignore the rest of the input line.
			}
			if(tokenizer.hasNext()) {
				word3 = tokenizer.next();      // get thirdword
				// note: we just ignore the rest of the input line.
			}
		}

		// Now check whether this word is known. If so, create a command
		// with it. If not, create a "null" command (for unknown command).
		if(commands.isCommand(word1)) {
			return new Command(word1, word2, word3);
		}
		else {
			return new Command(null, word2, word3); 
		}
	}
	/**
	 * Print out a list of valid command words.
	 */
	public void showCommands()
	{
		commands.showAll();
	}
}
