package com.planetjumper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import java.net.*;
import javax.jnlp.*;

/**
 * This class is part of the "Planet Jumper" application. 
 * "Planet Jumper" is a text based game that involves exploring the solar system in order to 
 * find minerals and bring them back to earth to help the survival of humanity. Users can
 * jump from planets to moons in the solar system searching for minerals and resources. 
 * To win this game the user must find a quantity of 100 gold, silver and copper then be 
 * able to make it back to earth with enough fuel, water and air to survive the journey. 
 * Exploring the solar to give the minerals to the people of earth will help the human race 
 * from depleting their minerals.
 * 
 * The challenging tasks that were implemented into this game are three-word command words such as get Gold 5,
 * and the jump command that calculates how much resources will be taken up during each jump to other planets and moons.
 * A challenging task could also include the feature that displays a map and shows the user the currentPlanet and their moons.
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  planets, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Bryn Flewelling
 * @version 2010.11.2
 */
public class Game 
{
	// static fields:
	private static final String VERSION = "Date 2010.11.2";

	// fields:
	private Parser parser;
	private Planet currentPlanet;
	private Moon currentMoon;
	private HashMap<String, Planet> planets; 
	private Fuel fuel;
	private Air air;
	private Water water;
	private Gold gold;
	private Silver silver;
	private Copper copper;
	private Planet previousPlanet;
	private Planet start;
	private int totalFuel =100;
	private int totalAir=100;
	private int totalWater=100;
	private int totalGold=0;
	private int totalSilver=0;
	private int totalCopper=0;

	private JLabel label = new JLabel(new ImageIcon("resources/Planet_Earth_by_sanmonku.jpg"));;
	private JFrame frame;
	private JFrame frame1;
	//private ImagePanel imagePanel;
	private JButton button;
	static protected JTextArea dialog;
	static protected JTextArea details;
	//private JSeparator seperator;
	private JLabel fuelLabel;
	private JLabel airLabel;
	private JLabel waterLabel;
	private JTextArea displayFuel;
	private JTextArea displayAir;
	private JTextArea displayWater;
	private JLabel goldLabel;
	private JLabel silverLabel;
	private JLabel copperLabel;
	private JTextArea displayGold;
	private JTextArea displaySilver;
	private JTextArea displayCopper;
	static BasicService basicService = null;
	/**
	 * Create the game and initialise its internal map.
	 */
	public Game() 
	{

		planets = new HashMap<String, Planet>();
		createPlanets();
		parser = new Parser();
		//resources
		fuel= new Fuel(100, "Fuel"); 
		air= new Air(100, "Air"); 
		water= new Water(100, "Water");
		//minerals
		gold= new Gold(0, "Gold"); 
		silver= new Silver(0, "Silver"); 
		copper= new Copper(0, "Copper");
		//makes the frames
		makeFrame();
		makeFrame1();
		
	    try {
	        basicService = (BasicService)
	          ServiceManager.lookup("javax.jnlp.BasicService");
	      } catch (UnavailableServiceException e) {
	        System.err.println("Lookup failed: " + e);
	      }
		
	}

	/**
	 * Create all the items, plants, moons and link their coordinates together.
	 */
	private void createPlanets()
	{
		Planet  mercury, venus, earth, mars, jupiter, saturn, neptune, uranus, pluto;

		// create the planets
		mercury = new Planet("Mercury");     
		venus = new Planet("Venus");
		earth = new Planet("Earth");
		mars = new Planet("Mars");
		jupiter = new Planet("Jupiter");
		saturn = new Planet("Saturn");
		neptune = new Planet("Nepturn");
		uranus = new Planet("Uranus");
		pluto = new Planet("Pluto");
		//-----------------------------------------------//  
		//initialise planet coordinates
		mercury.setCoordinate("Venus", venus);
		//venus.setCoordinate("Earth", earth);
		earth.setCoordinate("Mars", mars);
		mars.setCoordinate("Jupiter", jupiter);
		jupiter.setCoordinate("Saturn", saturn);
		saturn.setCoordinate("Uranus", uranus);
		neptune.setCoordinate("Neptune", neptune);
		uranus.setCoordinate("Pluto", pluto);
		pluto.setCoordinate("Mercury", mercury);
		//--initialise planet coordinates(continued)--//
		mercury.setCoordinate("Pluto", pluto);
		venus.setCoordinate("Mercury", mercury);
		//earth.setCoordinate("Venus", venus);
		mars.setCoordinate("Earth", earth);
		jupiter.setCoordinate("Mars", mars);
		neptune.setCoordinate("Neptune", neptune);
		saturn.setCoordinate("Jupiter", jupiter);
		uranus.setCoordinate("Saturn", saturn);
		pluto.setCoordinate("Uranus", uranus); 
		//-----------------------------------------------//    
		//initialise planet moons
		Moon moon = new Moon("Moon", earth);
		Moon deimos = new Moon("Deimos", mars);
		Moon phobos = new Moon("Phobos", mars);
		Moon ganymede = new Moon("Ganymede", jupiter);
		Moon europa = new Moon("Europa", jupiter);
		Moon io = new Moon("Io", jupiter);
		Moon callisto = new Moon("Callisto", jupiter);
		Moon titan = new Moon("Titan", saturn);
		Moon rhea = new Moon("Rhea", saturn);
		Moon iapetus = new Moon("Iapetus", saturn);
		Moon dione = new Moon("Dione", saturn);
		Moon proteus = new Moon("Proteus", neptune);
		Moon larissa = new Moon("Larissa", neptune);
		Moon ariel = new Moon("Ariel", uranus);
		Moon oberon = new Moon("Oberon", uranus);
		Moon titania = new Moon("Titania", uranus);
		Moon umbriel = new Moon("Umbriel", uranus);
		Moon charon = new Moon("Charon", pluto); 
		//-----------------------------------------------// 
		//set the moon coordinates
		moon.setCoordinate("Earth", earth);
		deimos.setCoordinate("Mars", mars);
		phobos.setCoordinate("Mars", mars);
		ganymede.setCoordinate("Jupiter", jupiter);
		europa.setCoordinate("Jupiter", jupiter);
		io.setCoordinate("Jupiter", jupiter);
		callisto.setCoordinate("Jupiter", jupiter); 
		titan.setCoordinate("Saturn", saturn);
		rhea.setCoordinate("Saturn", saturn);
		iapetus.setCoordinate("Saturn", saturn);
		dione.setCoordinate("Saturn", saturn);
		proteus.setCoordinate("Neptune", neptune);
		larissa.setCoordinate("Neptune", neptune);
		ariel.setCoordinate("Uranus", uranus);
		oberon.setCoordinate("Uranus", uranus);
		titania.setCoordinate("Uranus", uranus);
		umbriel.setCoordinate("Uranus", uranus);
		charon.setCoordinate("Pluto", pluto);

		//set the moons planet
		earth.setMoon(moon);
		mars.setMoon(deimos);
		mars.setMoon(phobos);
		jupiter.setMoon(ganymede);
		jupiter.setMoon(europa);
		jupiter.setMoon(io);
		jupiter.setMoon(callisto);
		saturn.setMoon(titan);
		saturn.setMoon(rhea);
		saturn.setMoon(iapetus);
		saturn.setMoon(dione);
		neptune.setMoon(proteus);
		neptune.setMoon(larissa);
		uranus.setMoon(ariel);
		uranus.setMoon(oberon);
		uranus.setMoon(titania);
		uranus.setMoon(umbriel);
		pluto.setMoon(charon);
		//-----------------------------------------------// 
		//initialise resources on planets and moons
		//gold
		Gold gold1 = new Gold(10, "Gold");
		moon.setGold(gold1);
		Gold gold2 = new Gold(20, "Gold");
		phobos.setGold(gold2);
		Gold gold3 = new Gold(20, "Gold");
		proteus.setGold(gold3);
		Gold gold4 = new Gold(10, "Gold");
		oberon.setGold(gold4);
		Gold gold5 = new Gold(10, "Gold");
		umbriel.setGold(gold5);
		Gold gold6 = new Gold(5, "Gold");
		pluto.setGold(gold6);
		Gold gold7 = new Gold(20, "Gold");
		iapetus.setGold(gold7);
		Gold gold8 = new Gold(10, "Gold");
		europa.setGold(gold8);	
		Gold gold9 = new Gold(5, "Gold");
		ariel.setGold(gold9);
		Gold gold10 = new Gold(10, "Gold");
		io.setGold(gold10);	
		//silver
		Silver silver1 = new Silver(10, "Silver");
		deimos.setSilver(silver1);
		Silver silver2 = new Silver(10, "Silver");
		ganymede.setSilver(silver2);
		Silver silver3 = new Silver(20, "Silver");
		iapetus.setSilver(silver3);
		Silver silver4 = new Silver(5, "Silver");
		titania.setSilver(silver4);
		Silver silver5 = new Silver(10, "Silver");
		larissa.setSilver(silver5);
		Silver silver6 = new Silver(5, "Silver");
		dione.setSilver(silver6);
		Silver silver7 = new Silver(10, "Silver");
		rhea.setSilver(silver7);
		Silver silver8 = new Silver(20, "Silver");
		callisto.setSilver(silver8);
		Silver silver9 = new Silver(10, "Silver");
		mars.setSilver(silver9);
		Silver silver10 = new Silver(10, "Silver");
		neptune.setSilver(silver10);
		//copper        
		Copper copper1 = new Copper(10, "Copper");
		larissa.setCopper(copper1);
		Copper copper2 = new Copper(10, "Copper");
		saturn.setCopper(copper2);
		Copper copper3 = new Copper(5, "Copper");
		uranus.setCopper(copper3);
		Copper copper4 = new Copper(5, "Copper");
		oberon.setCopper(copper4);
		Copper copper5 = new Copper(10, "Copper");
		neptune.setCopper(copper5);   
		Copper copper6 = new Copper(5, "Copper");
		phobos.setCopper(copper6); 
		Copper copper7 = new Copper(10, "Copper");
		ganymede.setCopper(copper7); 
		Copper copper8 = new Copper(5, "Copper");
		ariel.setCopper(copper8); 
		Copper copper9 = new Copper(10, "Copper");
		charon.setCopper(copper9); 
		Copper copper10 = new Copper(5, "Copper");
		dione.setCopper(copper10); 
		Copper copper11 = new Copper(10, "Copper");
		mars.setCopper(copper11); 
		Copper copper12 = new Copper(10, "Copper");
		moon.setCopper(copper12); 
		Copper copper13 = new Copper(10, "Copper");
		rhea.setCopper(copper13); 
		Copper copper14 = new Copper(10, "Copper");
		callisto.setCopper(copper14);
		Copper copper15 = new Copper(20, "Copper");
		io.setCopper(copper15); 
		//-----------------------------------------------// 
		//initialise planet distances from sun(determines how much fuel is used when jumping)
		mercury.setDistanceFromSun(360);
		venus.setDistanceFromSun(67);
		earth.setDistanceFromSun(93);
		mars.setDistanceFromSun(142);
		jupiter.setDistanceFromSun(484);
		saturn.setDistanceFromSun(891);
		neptune.setDistanceFromSun(2800);
		uranus.setDistanceFromSun(1790);
		pluto.setDistanceFromSun(93); 
		//-----------------------------------------------//
		//planets
		planets.put("uranus", uranus);
		planets.put("pluto", pluto);
		planets.put("mercury", mercury);
		planets.put("venus", venus);
		planets.put("earth", earth);
		planets.put("mars", mars);
		planets.put("neptune", neptune);
		planets.put("jupiter", jupiter);
		planets.put("saturn", saturn);
		//-----------------------------------------------//
		earth.setIsCurrentCoordinate(true);
		currentPlanet = earth;  // start game at earth
		start = currentPlanet;
	}
	/**
	 * Displays the map to the user when the command word map is entered.
	 */    
	public void displayMap() {
		Set<String> keys = planets.keySet();
		Iterator<String> it = keys.iterator();
		details.append("Map : \n");
		details.append("===========================================================\n");
		while (it.hasNext()) {
			String planetName = (String) it.next();
			Planet planet = planets.get(planetName);
			if(planet.equals(currentPlanet ))
			{
				details.append("***Planet : " + planet.getShortName() + ", Moons : " + planet.displayMoons() + "\n");
			}else{
				details.append("Planet : " + planet.getShortName() + ", Moons : " + planet.displayMoons() + "\n");
			}
		}
		details.append("===========================================================\n");
		details.append("Current Planet : " + currentPlanet.getShortName() + "\n");
	}
	/**
	 *  Main play routine.  Loops until end of play or the user quits.
	 */
	public void play() 
	{            
		printWelcome();
		displayFuel.setText(totalFuel + "\n");
		displayAir.setText(totalAir + "\n");
		displayWater.setText(totalWater + "\n");

		displayGold.setText(totalGold + "\n");
		displaySilver.setText(totalSilver + "\n");
		displayCopper.setText(totalCopper + "\n");
		// Enter the main command loop.  Here we repeatedly read commands and
		// execute them until the game is over or the user quits.           
		boolean finished = false;
		while (! finished) {
			Command command = parser.getCommand();
			finished = processCommand(command);
			// cargo();
			if(start.getIsCurrentCoordinate()){
				if(totalGold != 100 & totalSilver != 100 & totalCopper != 100){
					details.append("================================================================\n");
					details.append("You need 100 Gold, Silver and Copper to accomplish your mission.\n");
					details.append("================================================================\n");
				}else{
					label.setIcon(new ImageIcon("resources/Moons/Planet_Earth_END.jpg"));
					details.append("Congradulations. You have completed your mission.\n");
					details.append("You have found successfully found 100 gold, silver and copper\n");
					details.append("================================================================\n");
					details.append("Thank you for playing. Good bye..\n");
					
				}
			}
		}  
		details.append("Thank you for playing.  Good bye.\n");
	}
	/**
	 * Print out the opening message(s) for the player.
	 */
	private void printWelcome()
	{
		details.append("\n");
		details.append("Welcome to Planet Hopper!\n");
		details.append("Planet Hopper is a game based on exploring the solar system.\n");
		details.append("Type 'help' if you need help.\n");
		details.append("\n");
		details.append(currentPlanet.getLongName());
	}
	/**
	 * Given a command, process (that is: execute) the command.
	 * @param command The command to be processed.
	 * @return true If the command ends the game, false otherwise.
	 */
	private boolean processCommand(Command command) 
	{
		boolean wantToQuit = false;

		if(command.isUnknown()) {
			details.append("I don't know what you mean...\n");
			return false;
		}
		String commandWord = command.getCommandWord();
		if (commandWord.equals("help")) {
			printHelp();
		}
		else if (commandWord.equals("jump")) {
			jump(command);
		}
		else if (commandWord.equals("display")) {
			displayMoon(command);
		}
		else if (commandWord.equals("scan")) {
			scan(command);
		}
		else if (commandWord.equals("map")) {
			displayMap();
		}
		else if (commandWord.equals("quit")) {
			wantToQuit = quit(command);
		}
		else if (commandWord.equals("get")) {
			get(command);
		}
		else if (commandWord.equals("back")) {
			back(command);
		}   
		else if (commandWord.equals("cargo")) {
			cargo();
		}   
		else if (commandWord.equals("scavenge")) {
			scavenge(command);
		}  
		// else command not recognised.
		return wantToQuit;
	}
	/**
	 * Print out the cargo(resources and minerals) for the user.
	 */
	private void cargo() {
		details.append("================================================================\n");
		details.append("Resources : \n");
		details.append("Fuel : " + fuel.getQuantity() + "\n");
		details.append("Air : " + air.getQuantity() + "\n");
		details.append("Water : " + water.getQuantity() + "\n");
		details.append("Minerals : \n");
		details.append("Gold : " + gold.getQuantity() + "\n");
		details.append("Silver : " + silver.getQuantity() + "\n");
		details.append("Copper : " + copper.getQuantity() + "\n");
		details.append("================================================================\n");
	}
	/**
	 * Print out some help information.
	 * Here we print a cryptic message and list the 
	 * command words.
	 */
	private void printHelp() 
	{
		details.append("You are lost. You are alone. You fly \n");
		details.append("through the solar system in search of \n");
		details.append("resources and minerals.\n");
		details.append("=================================================================\n");
		details.append("Your command words are:\n");
		//parser.showCommands();
		details.append("=================================================================\n");
		details.append("Jump : jumps to a planet or a moon\n");
		details.append("Display : displays the avialable coordinates of a planet\n");
		details.append("Scan : scans a planet for resources or minerals\n");
		details.append("Get : gets a desired item. Example : get Gold 5\n");
		details.append("Map : displays a map to the user and shows the current position\n");
		details.append("Cargo : displays te resources and minerals the user has\n");
		details.append("Back : takes the user back to a previous planet\n");
	}
	/** 
	 * Try to jump to a coordinate. If there is an coordinate, enter the planet name
	 * ,or moon name otherwise print an error message. When you move from one coordinate to 
	 * another, resources(items) will be removed depending on the distance and the amount 
	 * of fuel, air, and water it required to make the jump. If their is not enough fuel
	 * error messages will be displayed.
	 * @param command The command to be processed.
	 */
	private void jump(Command command) 
	{
		if(!command.hasSecondWord()) {
			// if there is no second word, we don't know where to go...
			details.append("Go where?\n");
			return;
		}
		String name = command.getSecondWord();
		Planet nextPlanet = null;

		// Try to leave current planet and go to another planet.
		if (currentPlanet.getIsCurrentCoordinate()) {
			nextPlanet = currentPlanet.getCoordinate(name);

		} else if (currentMoon != null) {
			if (currentMoon.getIsCurrentCoordinate()) {
				nextPlanet = currentMoon.getCoordinate(name);
				if(nextPlanet == null){
					details.append("Invalid coordinate. Must be at a planet to jump to a moon.\n");
				}
			} 
		}
		// Item item = items.get("Fuel"); 
		totalFuel = fuel.getQuantity();
		totalAir = air.getQuantity();
		totalWater = water.getQuantity();
		totalGold = gold.getQuantity();
		totalSilver = silver.getQuantity();
		totalCopper = copper.getQuantity();

		boolean goodToGo = true;
		if (nextPlanet == null) {
			details.append("There is no planet! " + name + "\n");

			Moon nextMoon = currentPlanet.getMoon(name);
			if (nextMoon == null){
				details.append("There is no moon! " + name + "\n");
			}else{        			
				if (totalFuel - 30 < 0)
				{
					details.append("You currently do not have enough fuel to go there" + "\n");
					details.append("HINT: Try searching moons and planets to find more fuel\n");
					goodToGo = false;
				} else if (totalWater - 20 < 0)
				{
					details.append("You currently do not have enough water to reach that moon\n");
					details.append("HINT: Try searching planets and moons to find more water\n");
					goodToGo = false;
				} else if (totalAir - 10 < 0)
				{
					details.append("You currently do not have enough air to reach that moon\n");
					details.append("HINT: Try searching planets and moons to find more air\n");
					goodToGo = false;
				}
				if (goodToGo){
					previousPlanet = currentPlanet;
					currentPlanet.setIsCurrentCoordinate(false);
					if (currentMoon != null ) {
						currentMoon.setIsCurrentCoordinate(true);

						details.append("Reached moon destination : " + name + "\n");
						displayFuel.append(totalFuel + "\n");
						displayAir.append(totalAir + "\n");
						displayWater.append(totalWater + "\n");
					} else {    				
						details.append("Reached moon destination : " + name + "\n");       		     		             		
						currentMoon = nextMoon;
						currentMoon.setIsCurrentCoordinate(true);
						totalFuel = totalFuel - 30;
						totalWater = totalWater - 20;
						totalAir = totalAir - 10;
					}
				}
			}
		} else {
			//(Math.abs(currentPlanet.getDistanceFromSun() - nextPlanet.getDistanceFromSun())* .00001) < 0)
			if (totalFuel - 30 < 0) {
				details.append("You currently do not have enouph fuel to reach that planet\n");
				details.append("HINT: Try to go to planets moons to find fuel\n");
				goodToGo = false;
			} else {
				details.append("It will take 30 Fuel of " + totalFuel + "\n");
			}
			if (totalAir - 20 < 0) {
				details.append("You currently do not have enouph air to reach that planet\n");
				details.append("HINT: Try to go to planets moons to find more air\n");
				goodToGo = false;
			} else {
				details.append("It will take 20 Air of " + totalAir + "\n");
			}
			if (totalWater - 10 < 0) {
				details.append("You currently do not have enouph water to reach that planet\n");
				details.append("HINT: Try to go to planets moons to find water\n");
				details.append("================================================================\n");
				goodToGo = false;
			} else {
				details.append("It will take 10 Water of " + totalWater + "\n");
				details.append("================================================================\n");
				//details.append("It will take Fuel of " + Math.abs(Math.abs(currentPlanet.getDistanceFromSun() - nextPlanet.getDistanceFromSun())* .00001));
			} 
			if (goodToGo) {
				currentPlanet.setIsCurrentCoordinate(false);
				nextPlanet.setIsCurrentCoordinate(true);
				currentMoon = null;

				totalFuel = totalFuel - 30;
				totalAir = totalAir - 20;
				totalWater = totalWater - 10;

				previousPlanet = currentPlanet;

				currentPlanet = nextPlanet;
				details.append(currentPlanet.getLongName());
			}
		}
		fuel.setQuantity(totalFuel);
		air.setQuantity(totalAir);
		water.setQuantity(totalWater);
		details.append("Remaining Fuel :" + totalFuel + "\n");
		details.append("Remaining Air :" + totalAir + "\n");
		details.append("Remaining Water :" + totalWater + "\n");
	}
	/** 
	 * Takes the user back to the previous planet.
	 * @param command The command to be processed.
	 */
	private void back(Command command) {
		Command cmd = new Command("jump", previousPlanet.getShortName(), null);
		jump(cmd);
	}
	/** 
	 * Displays a list of the currentPlanets available moons
	 * ,else print error message
	 * @param command The command to be processed.
	 */
	private void displayMoon(Command command) 
	{
		if(currentPlanet.getIsCurrentCoordinate()){
			details.append(currentPlanet.displayMoons() + "\n");
		}else{
			details.append("You are already on a moon, please jump to a planet\n");
		}
	}
	/** 
	 * Scans a planet else a moon to discover what resources or items it contains
	 * @param command The command to be processed.
	 */
	private void scan(Command command) 
	{ 
		if (currentPlanet.getIsCurrentCoordinate()) {
			details.append("Resources available for pickup : \n");
			details.append("Fuel : " + currentPlanet.fuel.getQuantity() + "\n");
			details.append("Air : " + currentPlanet.air.getQuantity() + "\n");
			details.append("Water : " + currentPlanet.water.getQuantity() + "\n");

			details.append("Gold : " + currentPlanet.gold.getQuantity() + "\n");
			details.append("Silver : " + currentPlanet.silver.getQuantity() + "\n");
			details.append("Copper : " + currentPlanet.copper.getQuantity() + "\n");
		}else{
			if (currentMoon.getIsCurrentCoordinate()) {
				details.append("Fuel : " + currentMoon.fuel.getQuantity() + "\n");
				details.append("Air : " + currentMoon.air.getQuantity() + "\n");
				details.append("Water : " + currentMoon.water.getQuantity() + "\n");

				details.append("Gold : " + currentMoon.gold.getQuantity() + "\n");
				details.append("Silver : " + currentMoon.silver.getQuantity() + "\n");
				details.append("Copper : " + currentMoon.copper.getQuantity() + "\n");
			}
		}
	}
	/** 
	 * Gets a planet or a moons resources, minerals or items it contains
	 * ,otherwise print an error message
	 * @param command The command to be processed.
	 */
	public void get(Command command)
	{
		if(!command.hasSecondWord()) {
			// if there is no second word, we don't know where to go...
			details.append("invalid command?\n");
			return;
		}
		if(!command.hasThirdWord()) {
			// if there is no thirdWord word, we don't know what to do...
			details.append("what?\n");
			return;
		}
		String name = command.getSecondWord();
		int quantity = Integer.parseInt(command.getThirdWord());

		if (currentPlanet.getIsCurrentCoordinate()) {
			if(name.equals("Fuel"))
			{
				int totalFuel1 = currentPlanet.getFuel(quantity);
				fuel.setQuantity(totalFuel1 + fuel.getQuantity());
			}
			if(name.equals("Air"))
			{
				int totalAir1 =	currentPlanet.getAir(quantity);
				air.setQuantity(totalAir1 + air.getQuantity());
			}
			if(name.equals("Water"))
			{
				int totalWater1 = currentPlanet.getWater(quantity);
				water.setQuantity(totalWater1 + water.getQuantity());
			}
			if(name.equals("Gold"))
			{
				int totalGold1 = currentPlanet.getGold(quantity);
				gold.setQuantity(totalGold1 + gold.getQuantity());
			}
			if(name.equals("Silver"))
			{
				int totalSilver1 = currentPlanet.getSilver(quantity);
				silver.setQuantity(totalSilver1 + silver.getQuantity());
			}
			if(name.equals("Copper"))
			{
				int totalCopper1 =	currentPlanet.getCopper(quantity);
				copper.setQuantity(totalCopper1 + copper.getQuantity());
			}
		}else{
			if (currentMoon.getIsCurrentCoordinate()) {
				if(name.equals("Fuel"))
				{
					int totalFuel1 = currentMoon.getFuel(quantity);
					fuel.setQuantity(totalFuel1 + fuel.getQuantity());
				}
				if(name.equals("Air"))
				{
					int totalAir1 = currentMoon.getAir(quantity);
					air.setQuantity(totalAir1 + air.getQuantity());
				}
				if(name.equals("Water"))
				{
					int totalWater1 = currentMoon.getWater(quantity);
					water.setQuantity(totalWater1 + water.getQuantity());
				}
				if(name.equals("Gold"))
				{
					int totalGold1 = currentMoon.getGold(quantity);
					gold.setQuantity(totalGold1 + gold.getQuantity());
				}
				if(name.equals("Silver"))
				{
					int totalSilver1 = currentMoon.getSilver(quantity);
					silver.setQuantity(totalSilver1 + silver.getQuantity());
				}
				if(name.equals("Copper"))
				{
					int totalCopper1 = currentMoon.getCopper(quantity);
					copper.setQuantity(totalCopper1 + copper.getQuantity());
				}
			}
		}
	}
	/** 
	 * Gets all of a planet or a moons resources, minerals or items it contains
	 * ,otherwise print an error message
	 * @param command The command to be processed.
	 */
	private void scavenge(Command command) 
	{
		if (currentPlanet.getIsCurrentCoordinate()) {
			int totalFuel1 = currentPlanet.getFuel(100);
			fuel.setQuantity(totalFuel1 + fuel.getQuantity());

			int totalAir1 =	currentPlanet.getAir(100);
			air.setQuantity(totalAir1 + air.getQuantity());

			int totalWater1 = currentPlanet.getWater(100);
			water.setQuantity(totalWater1 + water.getQuantity());

			int totalGold1 = currentPlanet.getGold(100);
			gold.setQuantity(totalGold1 + gold.getQuantity());

			int totalSilver1 = currentPlanet.getSilver(100);
			silver.setQuantity(totalSilver1 + silver.getQuantity());

			int totalCopper1 =	currentPlanet.getCopper(100);
			copper.setQuantity(totalCopper1 + copper.getQuantity());
			details.append("====================================================\n");
		}else{
			if (currentMoon.getIsCurrentCoordinate()) {
				int totalFuel1 = currentMoon.getFuel(100);
				fuel.setQuantity(totalFuel1 + fuel.getQuantity());

				int totalAir1 =	currentMoon.getAir(100);
				air.setQuantity(totalAir1 + air.getQuantity());

				int totalWater1 = currentMoon.getWater(100);
				water.setQuantity(totalWater1 + water.getQuantity());

				int totalGold1 = currentMoon.getGold(100);
				gold.setQuantity(totalGold1 + gold.getQuantity());

				int totalSilver1 = currentMoon.getSilver(100);
				silver.setQuantity(totalSilver1 + silver.getQuantity());

				int totalCopper1 =	currentMoon.getCopper(100);
				copper.setQuantity(totalCopper1 + copper.getQuantity());
				details.append("====================================================\n");
			}
		}
	}
	/**
	 * Create the Swing frame and its content.
	 */
	private void makeFrame()
	{
		frame = new JFrame("PlanetJumper");
		JPanel contentPane = (JPanel)frame.getContentPane();
		contentPane.setBorder(new EmptyBorder(6, 6, 6, 6));   
		//contentPane.setBackground(Color.black);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.repaint();
		makeMenuBar(frame);
		
		//sets frame position and boundary
		frame.setBounds ( 0 , 5 , 50 , 100 );
		frame.setResizable ( false );

		// Specify the layout manager with nice spacing
		contentPane.setLayout(new BorderLayout(6, 6));

		// Create the image pane in the center
		// imagePanel = new ImagePanel();
		//imagePanel.setBorder(new EtchedBorder());
		//contentPane.add(imagePanel, BorderLayout.CENTER);

		//input label
		JLabel inputLabel = new JLabel("Input:");
		JPanel toolbarS = new JPanel();
		toolbarS.setLayout(new FlowLayout());
		//dialog text area
		dialog = new JTextArea(1, 40);
		toolbarS.add(inputLabel);
		toolbarS.add(dialog);
		contentPane.add(toolbarS, BorderLayout.SOUTH);

		// Create the toolbar with the buttons
		JPanel toolbarW = new JPanel();
		toolbarW.setLayout(new GridLayout(0, 1));

		button = new JButton("Jump");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				details.setText("");
				Command uiCommand = parser.parseInput("jump " + dialog.getText());
				jump(uiCommand);
				redrawPlanet();
				displayFuel.setText(totalFuel + "\n");
				displayAir.setText(totalAir + "\n");
				displayWater.setText(totalWater + "\n");

				displayGold.setText(totalGold + "\n");
				displaySilver.setText(totalSilver + "\n");
				displayCopper.setText(totalCopper + "\n");
			}
		});
		toolbarW.add(button);

		button = new JButton("Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				if (previousPlanet != null ) {
					details.setText("");
					Command uiCommand = parser.parseInput("back " + dialog.getText());
					back(uiCommand);
					redrawPlanet();
					displayFuel.setText(totalFuel + "\n");
					displayAir.setText(totalAir + "\n");
					displayWater.setText(totalWater + "\n");

					displayGold.setText(totalGold + "\n");
					displaySilver.setText(totalSilver + "\n");
					displayCopper.setText(totalCopper + "\n");
				}else{
					details.setText("");
					details.setText("Cannot go back from here. Please go to a planet or moon first.");
				}
			}
		});
		toolbarW.add(button);

		button = new JButton("Display");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				details.setText("");
				Command uiCommand = parser.parseInput("display " + dialog.getText());
				displayMoon(uiCommand);
				redrawPlanet();
				displayFuel.setText(totalFuel + "\n");
				displayAir.setText(totalAir + "\n");
				displayWater.setText(totalWater + "\n");

				displayGold.setText(totalGold + "\n");
				displaySilver.setText(totalSilver + "\n");
				displayCopper.setText(totalCopper + "\n");
			}
		});
		toolbarW.add(button);

		button = new JButton("Scan");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				details.setText("");
				Command uiCommand = parser.parseInput("sacn " + dialog.getText());
				scan(uiCommand);
				redrawPlanet();
				displayFuel.setText(totalFuel + "\n");
				displayAir.setText(totalAir + "\n");
				displayWater.setText(totalWater + "\n");

				displayGold.setText(totalGold + "\n");
				displaySilver.setText(totalSilver + "\n");
				displayCopper.setText(totalCopper + "\n");
			}
		});
		toolbarW.add(button);

		button = new JButton("Get");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				details.setText("");
				Command uiCommand = parser.parseInput("get " + dialog.getText());
				get(uiCommand);
				redrawPlanet();
				displayFuel.setText(totalFuel + "\n");
				displayAir.setText(totalAir + "\n");
				displayWater.setText(totalWater + "\n");

				displayGold.setText(totalGold + "\n");
				displaySilver.setText(totalSilver + "\n");
				displayCopper.setText(totalCopper + "\n");
			}
		});
		toolbarW.add(button);

		button = new JButton("Cargo");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { details.setText(""); cargo(); }
		});
		toolbarW.add(button);

		button = new JButton("Map");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { details.setText("");
			displayMap(); 
			displayFuel.setText(totalFuel + "\n");
			displayAir.setText(totalAir + "\n");
			displayWater.setText(totalWater + "\n");

			displayGold.setText(totalGold + "\n");
			displaySilver.setText(totalSilver + "\n");
			displayCopper.setText(totalCopper + "\n");
			}
		});
		toolbarW.add(button);

		//display resources
		fuelLabel = new JLabel("Fuel");
		contentPane.add(fuelLabel, BorderLayout.WEST);
		toolbarW.add(fuelLabel);
		displayFuel = new JTextArea();
		displayFuel.setEditable(false);
		toolbarW.add(displayFuel);

		airLabel = new JLabel("Air");
		contentPane.add(airLabel, BorderLayout.WEST); 
		toolbarW.add(airLabel);
		displayAir = new JTextArea();
		displayAir.setEditable(false);
		toolbarW.add(displayAir);

		waterLabel = new JLabel("Water");
		contentPane.add(waterLabel, BorderLayout.WEST);
		toolbarW.add(waterLabel);
		displayWater = new JTextArea();
		displayWater.setEditable(false);
		toolbarW.add(displayWater);

		// Add toolbar into panel with flow layout for spacing
		JPanel flowW = new JPanel();
		flowW.add(toolbarW);

		contentPane.add(flowW, BorderLayout.WEST);

		// Create the toolbar with the buttons
		JPanel toolbarE = new JPanel();
		toolbarE.setLayout(new GridLayout(0, 1));

		button = new JButton("Scavenge");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				details.setText("");
				Command uiCommand = parser.parseInput("scavenge ");
				scavenge(uiCommand);
				redrawPlanet();	
				displayFuel.setText(totalFuel + "\n");
				displayAir.setText(totalAir + "\n");
				displayWater.setText(totalWater + "\n");

				displayGold.setText(totalGold + "\n");
				displaySilver.setText(totalSilver + "\n");
				displayCopper.setText(totalCopper + "\n");
			}
		});
		toolbarE.add(button);

		button = new JButton("Help");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { details.setText("");printHelp(); }
		});
		toolbarE.add(button);

		button = new JButton("Quit");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				Command uiCommand = parser.parseInput("quit ");
				quit(uiCommand);
				System.exit(0); //call this method to quit jFrame properly
				redrawPlanet();
			}
		});
		toolbarE.add(button);
		//displays values of current resources and minerals
		goldLabel = new JLabel("Gold");
		contentPane.add(goldLabel, BorderLayout.EAST);
		toolbarE.add(goldLabel);
		displayGold = new JTextArea();
		displayGold.setEditable(false);
		toolbarE.add(displayGold);

		silverLabel = new JLabel("Silver");
		contentPane.add(silverLabel, BorderLayout.EAST); 
		toolbarE.add(silverLabel);
		displaySilver = new JTextArea();
		displaySilver.setEditable(false);
		toolbarE.add(displaySilver);

		copperLabel = new JLabel("Copper");
		contentPane.add(copperLabel, BorderLayout.EAST);
		toolbarE.add(copperLabel);
		displayCopper = new JTextArea();
		displayCopper.setEditable(false);
		toolbarE.add(displayCopper);

		// Add toolbar into panel with flow layout for spacing
		JPanel flowE = new JPanel();
		flowE.add(toolbarE);

		contentPane.add(flowE, BorderLayout.EAST);
		redrawPlanet();
		//image background load
		//if(currentPlanet.coordinate.containsKey("Earth")){
		/* JLabel label = null;
         if (currentPlanet.getShortName().equals("Earth")) {
        	 label = new JLabel(new ImageIcon("resources/Planet_Earth_by_sanmonku.jpg"));
         } else {
        	 label = new JLabel(new ImageIcon("resources/Mars_by_tehboriz.jpg"));
         }

         label.repaint();

         frame.getContentPane().add(label, BorderLayout.CENTER);
         // Use a label to display the image
         if (label != null){

         frame.getContentPane().add(label, BorderLayout.CENTER);
         frame.pack();
         frame.setVisible(true);

         }*/
		//}   
	}
	/**
	 * Create the Swing frame1 and its content.
	 */
	private void makeFrame1()
	{ 
		frame1 = new JFrame("PlanetJumperOutput");
		JPanel contentPane1 = (JPanel)frame1.getContentPane();
		contentPane1.setBorder(new EmptyBorder(6, 6, 6, 6));   
		frame1.setBackground(Color.black);
		//frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//sets frame position and boundary
		frame1.setBounds ( 730 , 5 , 50 , 100 );
		frame1.repaint();
		makeMenuBar(frame1);
		frame1.setResizable ( false );
		// Specify the layout manager with nice spacing
		contentPane1.setLayout(new BorderLayout(6, 6));
		//creates a jPanel called toolbarS, with a FlowLayout
		JPanel toolbarS = new JPanel();
		toolbarS.setLayout(new FlowLayout());
		//creates a new text area called details
		details = new JTextArea(25, 50);
		details.setEditable(false);;
		contentPane1.add(details);
		// Set up the area where the details will be displayed.
		/*    JTextArea details = new JTextArea(10, 50);
        details.setEditable(false);
        //details.setEnabled(false);
        JScrollPane scrollArea =
                new JScrollPane(details,
                                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        contentPane1.add(scrollArea, BorderLayout.CENTER);*/
		frame1.pack();
		frame1.setVisible(true); 
	}

	private void redrawPlanet() {
		//currentPlanet image background load
		if (currentPlanet.getShortName().equals("Earth")) {
			label.setIcon(new ImageIcon("resources/Planet_Earth_by_sanmonku.jpg"));
		}
		if (currentPlanet.getShortName().equals("Mars")){
			label.setIcon(new ImageIcon("resources/Mars_by_tehboriz.jpg"));
		}
		if (currentPlanet.getShortName().equals("Jupiter")){
			label.setIcon(new ImageIcon("resources/Jupiter.jpg"));
		}
		if (currentPlanet.getShortName().equals("Saturn")){
			label.setIcon(new ImageIcon("resources/saturn-299666main_pia11141a.jpg"));
		}
		if (currentPlanet.getShortName().equals("Uranus")){
			label.setIcon(new ImageIcon("resources/uranus.jpg"));
		}
		if (currentPlanet.getShortName().equals("Neptune")){
			label.setIcon(new ImageIcon("resources/neptune_ngm_20081027_1_1024.jpg"));
		}
		if (currentPlanet.getShortName().equals("Pluto")){
			label.setIcon(new ImageIcon("resources/pluto_by_luquid-d2ycw6i.jpg"));
		}
		if (currentPlanet.getShortName().equals("Mercury")){
			label.setIcon(new ImageIcon("resources/Mercury.jpg"));
		}
		if(currentMoon != null){
			//currentMoon image background load
			if (currentMoon.getShortName().equals("Moon")){
				label.setIcon(new ImageIcon("resources/Moons/moon-2.gif"));
			}
			if (currentMoon.getShortName().equals("Deimos")){
				label.setIcon(new ImageIcon("resources/Moons/Deimos-Mars-2.jpg"));
			}
			if (currentMoon.getShortName().equals("Phobos")){
				label.setIcon(new ImageIcon("resources/Moons/phobos-marte.jpg"));
			}
			if (currentMoon.getShortName().equals("Ganymede")){
				label.setIcon(new ImageIcon("resources/Moons/GANYMEDE___JUPITERS_MOON_by_mr_nerb.jpg"));
			}
			if (currentMoon.getShortName().equals("Europa")){
				label.setIcon(new ImageIcon("resources/Moons/europa_by_77mynameislol77-d30jfhg.png"));
			}
			if (currentMoon.getShortName().equals("Io")){
				label.setIcon(new ImageIcon("resources/Moons/io_by_77mynameislol77-d30jfn6.png"));
			}
			if (currentMoon.getShortName().equals("Callisto")){
				label.setIcon(new ImageIcon("resources/Moons/callisto_by_rah2005-d32f2h0.jpg"));
			}
			if (currentMoon.getShortName().equals("Titan")){
				label.setIcon(new ImageIcon("resources/Moons/Titan_in_natural_color_Cassini.jpg"));
			}
			if (currentMoon.getShortName().equals("Rhea")){
				label.setIcon(new ImageIcon("resources/Moons/rhea_fulldisc.jpg"));
			}
			if (currentMoon.getShortName().equals("Iapetus")){
				label.setIcon(new ImageIcon("resources/Moons/iapetus3_cassini_big.jpg"));
			}
			if (currentMoon.getShortName().equals("Dione")){
				label.setIcon(new ImageIcon("resources/Moons/dione.jpg"));
			}
			if (currentMoon.getShortName().equals("Proteus")){
				label.setIcon(new ImageIcon("resources/Moons/4_7f65b575f1535c08781b07a296cb1b99.jpg"));
			}
			if (currentMoon.getShortName().equals("Larissa")){
				label.setIcon(new ImageIcon("resources/Mons/4_a9f3ed54e868539d3ae82115cc0ed1d5.jpg"));
			}
			if (currentMoon.getShortName().equals("Ariel")){
				label.setIcon(new ImageIcon("resources/Moons/Valkano__Ariel_orbit___by_chavanuco.jpg"));
			}
			if (currentMoon.getShortName().equals("Oberon")){
				label.setIcon(new ImageIcon("resources/Moons/oberon.jpg"));
			}
			if (currentMoon.getShortName().equals("Titania")){
				label.setIcon(new ImageIcon("resources/Moons/titania.jpg"));
			}
			if (currentMoon.getShortName().equals("Umbriel")){
				label.setIcon(new ImageIcon("resources/Moons/umbrielcolorbest.jpg"));
			}
			if (currentMoon.getShortName().equals("Charon")){
				label.setIcon(new ImageIcon("resources/Moons/Pluto_and_Charon_by_sine_out.jpg"));
			}
		}
		label.repaint();

		frame.getContentPane().add(label, BorderLayout.CENTER);
		// Use a label to display the image
		if (label != null){

			frame.getContentPane().add(label, BorderLayout.CENTER);
			frame.pack();
			frame.setVisible(true);

		}
	}
	/**
	 * Create the main frame's menu bar.
	 * 
	 * @param frame   The frame that the menu bar should be added to.
	 */
	private void makeMenuBar(JFrame frame2)
	{
		final int SHORTCUT_MASK =
			Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

		JMenuBar menubar = new JMenuBar();
		frame2.setJMenuBar(menubar);

		JMenu menu;
		JMenuItem item;

		// create the File menu
		menu = new JMenu("File");
		menubar.add(menu);

		 /*  item = new JMenuItem("New Game");
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, SHORTCUT_MASK));
        item.addActionListener(new ActionListener() {
                           public void actionPerformed(ActionEvent e) {
               			//	Command uiCommand = parser.parseInput("quit ");
            				//quit(uiCommand);
            				//main(null);
            				//System.exit(0); //call this method to quit jFrame properly
            				//redrawPlanet();
                        	  // RunTime.getRuntime().exec("java My_Program");
                           }
                       });
        menu.add(item);
*/
		//creates the output panel option
		item = new JMenuItem("Output Panel");
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, SHORTCUT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(frame1.isActive() == false){
					makeFrame1(); 
				}else{
					details.setText("Frame is already open");
				}

			}
		});
		menu.add(item);

		//creates the clear option
		item = new JMenuItem("Clear");
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, SHORTCUT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialog.setText("");
				details.setText("");
			}
		});
		menu.add(item);

		//creates the quit option
		item = new JMenuItem("Quit");
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, SHORTCUT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { //dialog.getText();
				Command uiCommand = parser.parseInput("quit ");
				quit(uiCommand);
				System.exit(0); //call this method to quit jFrame properly
				redrawPlanet();
			}
		});
		menu.add(item);

		// create the File menu
		menu = new JMenu("Options");
		menubar.add(menu);

		/*item = new JMenuItem("Font");
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, SHORTCUT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// create JFontChooser using 'font' as initial font
				// JFontChooser jFontChooser = new JFontChooser(font);
				 // show font chooser dialog and get the result
				// int result = jFontChooser.showDialog(parent);
				 // retrieve selected font
				 //Font selectedFont = null;
				 //if (result == APPROVE_OPTION)
				   //  selectedFont = jFontChooser.getSelectedFont();
				//details.setFont(selectedFont);
				//dialog.setFont(selectedFont);
			}
		});

		menu.add(item); */

		//creates the text colour option
		item = new JMenuItem("Text Colour");
		item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, SHORTCUT_MASK));
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//JColorChooser
				Color newColor = JColorChooser.showDialog(frame1, "Colour Chooser", details.getBackground());
				details.setForeground(newColor);
				dialog.setForeground(newColor);
			}
		});
		menu.add(item);

		// create the Help menu
		menu = new JMenu("Help");
		menubar.add(menu);
		
		item = new JMenuItem("Commands");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { details.setText("");printHelp(); }
		});
		menu.add(item);

		item = new JMenuItem("About PlanetJumper");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { showAbout(); }
		});
		menu.add(item);
	}
	/**
	 * 'About' function: show the 'about' box.
	 */
	private void showAbout()
	{
		JOptionPane.showMessageDialog(frame, 
				"PlanetJumper\nWritten By Bryn Flewelling\n" + VERSION,
				"About PlanetJumper\n", 
				JOptionPane.INFORMATION_MESSAGE);
	}
	/** 
	 * "Quit" was entered. Check the rest of the command to see
	 * whether we really quit the game.
	 * @return true, if this command quits the game, false otherwise.
	 */
	private boolean quit(Command command) 
	{
		return true;  // signal that we want to quit
	}
	/** 
	 * Main. Creates a instance of game and calls the play method.
	 */
	public static void main(String [ ] args)
	{
		Game game = new Game();
		game.play();
	}
}
