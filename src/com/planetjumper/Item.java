package com.planetjumper;

/**
 * Class Item - contains the quantity and the itemName of items in the solar system.
 *
 * This class is part of the "Planet Jumper" application. 
 * "Planet Jumper" is a text based game that involves exploring the solar system in order to 
 * find minerals and bring them back to earth to help the survival of humanity. Users can
 * jump from planets to moons in the solar system searching for minerals and resources. 
 * To win this game the user must find a quantity of 100 gold, silver and copper then be 
 * able to make it back to earth with enough fuel, water and air to survive the journey. 
 * Exploring the solar to give the minerals to the people of earth will help the human race 
 * from depleting their minerals.  
 *
 * A "Room" represents one location in the scenery of the game.  It is 
 * connected to other rooms via exits.  For each existing exit, the room 
 * stores a reference to the neighbouring room.
 * 
 * @author  Bryn Flewelling
 * @version 2010.10.07
 */

public class Item {

	protected int quantity;
	protected String itemName;

	public Item(int quantity, String itemName)
	{
		this.quantity = quantity;
		this.itemName = itemName;
	}
	/**
	 * Sets the quantity.
	 * @param quantity The quantity.
	 */
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;

	}
	/**
	 * Sets the items name.
	 * @param itemName The name of an item.
	 */
	public void setItemName(String itemName)
	{
		this.itemName = itemName;
	}
	/**
	 * Gets the quantity.
	 * @return quantity. 
	 */
	public int getQuantity()
	{
		return quantity;
	}
	/**
	 * Gets the items name.
	 * @return itemName. 
	 */
	public String getItemName()
	{
		return itemName;
	}
}
