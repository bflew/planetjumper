package com.planetjumper;

/**
 * This class is part of the "Planet Jumper" application. 
 * "Planet Jumper" is a text based game that involves exploring the solar system in order to 
 * find minerals and bring them back to earth to help the survival of humanity. Users can
 * jump from planets to moons in the solar system searching for minerals and resources. 
 * To win this game the user must find a quantity of 100 gold, silver and copper then be 
 * able to make it back to earth with enough fuel, water and air to survive the journey. 
 * Exploring the solar to give the minerals to the people of earth will help the human race 
 * from depleting their minerals.  
 *
 * This class holds information about a command that was issued by the user.
 * A command currently consists of two strings: a command word and a second
 * word (for example, if the command was "take map", then the two strings
 * obviously are "take" and "map").
 * 
 * The way this is used is: Commands are already checked for being valid
 * command words. If the user entered an invalid command (a word that is not
 * known) then the command word is <null>.
 *
 * If the command had only one word, then the second word is <null>.
 * 
 * @author  Bryn Flewelling
 * @version 2010.10.07
 */

public class Command
{
	private String commandWord;
	private String secondWord;
	private String thirdWord;
	/**
	 * Create a command object. First and second word must be supplied, but
	 * either one (or both) can be null.
	 * @param firstWord The first word of the command. Null if the command
	 *                  was not recognised.
	 * @param secondWord The second word of the command.
	 */
	public Command(String firstWord, String secondWord, String thirdWord)
	{
		commandWord = firstWord;
		this.secondWord = secondWord;
		this.thirdWord = thirdWord; 
	}


	/**
	 * @return The second word of this command. Returns null if there was no
	 * second word.
	 */
	public void setFirstWord(String word)
	{
		commandWord = word;
	}
	/**
	 * @return The second word of this command. Returns null if there was no
	 * second word.
	 */
	public void setSecondWord(String word)
	{
		secondWord = word;
	}
	/**
	 * @return The second word of this command. Returns null if there was no
	 * second word.
	 */
	/**
	 * @return The second word of this command. Returns null if there was no
	 * second word.
	 */
	public void setThirdWord(String word)
	{
		thirdWord = word;
	}
	/**
	 * Return the command word (the first word) of this command. If the
	 * command was not understood, the result is null.
	 * @return The command word.
	 */
	public String getCommandWord()
	{
		return commandWord;
	}
	public String getSecondWord()
	{
		return secondWord;
	}
	/**
	 * @return The second word of this command. Returns null if there was no
	 * second word.
	 */
	public String getThirdWord()
	{
		return thirdWord;
	}
	/**
	 * @return true if this command was not understood.
	 */
	public boolean isUnknown()
	{
		return (commandWord == null);
	}

	/**
	 * @return true if the command has a second word.
	 */
	public boolean hasSecondWord()
	{
		return (secondWord != null);
	}
	/**
	 * @return true if the command has a second word.
	 */
	public boolean hasThirdWord()
	{
		return (thirdWord != null);
	}
}

