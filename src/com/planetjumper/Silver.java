package com.planetjumper;

/**
	 * Class Silver - contains the quantity and the ItemName of silver.
	 *
	 * This class is part of the "Planet Hopper" application. 
	 * "Planet Hopper" is a super awesome, text based space exploration game.   
	 *
	 * A "Room" represents one location in the scenery of the game.  It is 
	 * connected to other rooms via exits.  For each existing exit, the room 
	 * stores a reference to the neighboring room.
	 * 
	 * @author  Bryn Flewelling
	 * @version 2010.10.07
	 */

public class Silver extends Item {

	public Silver(int quantity, String itemName) {
		super(quantity, itemName);
	}
}
